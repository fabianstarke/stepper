/* eslint-disable eqeqeq */
import React, { Component } from "react";

import CheckValid from "../images/checkValid.svg";
import styled from "styled-components";
import * as variables from "./variables";
const { SUCCESS, BLACK, GREY_2, GREY_6 } = variables;

const Wrapper = styled.div`
    position: absolute;
    left: 0px;
    width: max-content;
    height: auto;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`;

const NavBar = styled.div`
    width: 100%;
    margin: 0px auto 10px auto;
    text-align: center;
    display: flex;
`;

const Step = styled.div`
       &.hide {
            display: none;
        } 
`;
const Circle = styled.div`
    display: inline-block;
    width: 24px;
    height: 24px;
    border-radius: 24px;
    &.done {
        border: 1px solid ${SUCCESS};
    }
    &.previous {
        background-color: red !important;
    }

    &.span {
        color: red;
    }

    &.current {
        border: 1px solid ${BLACK};
        &.hide {
            display: inline-block !important;
        }
    }

    background: white;
    border: 1px solid ${GREY_6};
    position: relative;
    cursor: pointer;
`;

const Bar = styled.span`
    display: inline-block;
    width: 24px;
    height: 24px;
    border-radius: 24px;
    &.done {
        border: 1px solid ${SUCCESS};
    }

    &.hide {
        display: none;
    }

    &.current {
        &.hide {
            display: inline-block !important;
        }
    }
    &.small {
        width: 110px;
    }

    position: relative;
    border: 1px solid ${GREY_2};
    width: 178px;
    height: 0;
    top: -10px;
    margin-right: -5px;
    border-left: none;
    border-right: none;
    border-radius: 0;
`;

const Title = styled.span`
    color: ${BLACK};
    font-size: 14px;
    line-height: 19px;
    width: 160px;
    left: 50%;
    margin-left: -80px;
    position: absolute;
    top: 30px;
`;

const Index = styled.span`
    display: inline-block;
    width: 24px;
    height: 24px;
    line-height: 24px;
    border-radius: 50%;
    color: ${GREY_6};
    font-family: "Open Sans";
    font-size: 14px;
    padding: inherit;
    position: absolute;
    left: -0;
    svg {
        height: 11px;
        width: 11px;
        g {
            stroke: #ffffff;
        }
    }
    &.done {
        background-color: ${SUCCESS};
    }
    &.current {
        color: ${BLACK};
    }
    &.previous {
        background: white;
    }
`;

class Stepper extends Component {
    constructor(props) {
        super(props);
        this.handleNavClick = this.handleNavClick.bind(this);
        this.container = React.createRef();
        this.steps = [];

        this.state = {
            currentIndex: this.props.currentIndex || 0,
            width: 0,
            parentWidth: 0,
            wrap: null,
            difference: null,
            steps: [],
        };
    }

    componentDidMount() {
        this.setState({
            width: this.container.current.offsetWidth,
        });
        window.addEventListener("resize", this.onResize.bind(this));
        this.onResize();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.width != this.state.width) {
            this.setState({
                width: this.container.current.offsetWidth,
            });
        }

        if (
            prevState.width != this.state.width ||
            prevState.parentWidth != this.state.parentWidth
        ) {
            let wrap = this.state.parentWidth < this.state.width;
            this.setState({
                wrap: wrap,
            });
            console.log("changes");
            //this.handleStepsToShow(this.state.parentWidth);
        }

        if (prevProps.currentIndex !== this.props.currentIndex) {
            this.setState({
                currentIndex: this.props.currentIndex,
            });
        }
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.onResize);
    }

    onResize() {
        //console.log("resize"):
        const { width } = this.state;
        let parent = this.container.current.offsetParent.offsetWidth;
        let child = width != null ? width : this.container.current.offsetWidth;

        let wrap = parent < child;
        let difference = parent - child;
        //console.log("resize", parent, child, wrap, difference);

        this.setState({
            parentWidth: parent,
            wrap: wrap,
            difference: difference,
        });
    }

    setNavitems(navItems) {
        return navItems.map((title, index) => {
            let neighboorsRight = navItems.length - 1 - index;
            let neightboorsLeft = navItems.length - 1 - neighboorsRight;
            return {
                title,
                active: index === this.state.currentIndex ? true : false,
                done: index < this.state.currentIndex,
                neighboorsRight: neighboorsRight,
                neightboorsLeft: neightboorsLeft,
                show: "",
            };
        });
    }

    handleNavClick(e, title) {
        let titleIndex = this.props.navItems.indexOf(title);
        this.props.onChangePeriodIndex(e, titleIndex);
        this.setNavitems(this.props.navItems, titleIndex);
        //this.handleStepsToShow(this.state.parentWidth);
    }

    /*     handleStepsToShow(parent) {
        const { navItems } = this.props;
        let stepsWidth = this.container.current.offsetWidth;
        let totalsteps = navItems.length;
        let averageSteps = Math.round(totalsteps / 2);
        let difference = stepsWidth - parent;
        let stepWidth = Math.round(stepsWidth / totalsteps);

        const infos = {
            parent: parent ? parent : this.container.current.offsetParent.offsetWidth,
            totalSteps: totalsteps,
            stepsWidth: stepsWidth,
            stepWidth: stepWidth,
            difference: difference,
            average: averageSteps,
            numToRemove: Math.ceil(difference / stepWidth),
        };

        let items = this.setNavitems(navItems);

        let finalSteps = [];

        let itemsToShow = items;

        itemsToShow.map((step) => {
            if (difference > 0) {
                if (
                    !step.active ||
                    itemsToShow.indexOf(step).length - 1 ||
                    !step.done
                )
                    console.log("diff");

                //finalSteps.push(step)

                itemsToShow.splice(0, 1);
                console.log("itesm to show", itemsToShow);
                //console.log("final steps to show", finalSteps)
                //console.log("final steps to items", itemsToShow)
            } else console.log("nothing to remove");
        });

        console.log(infos);
    } */




    render() {
        const { navItems } = this.props;
        const { currentIndex, wrap, width, parentWidth } = this.state;

        let items = this.setNavitems(navItems, currentIndex);
        console.log(items);

        return (
            <Wrapper ref={this.container}>
                <NavBar>
                    {items.map((item, index) => (
                        <Step
                            id={`step-${index}`}
                            ref={(ref) => (this.steps[index] = ref)}
                            key={index}
                            /*   className={` ${
                                currentIndex > index
                                    ? "previous"
                                    : currentIndex < index
                                    ? "next"
                                    : "current"
                            }`} */
                      /*        className={
                                item.neightboorsLeft === 0 || items.indexOf(item) > currentIndex +3 ?  "hide" : "show"} */
                        >
                            <Circle
                                className={` ${
                                    item.active
                                        ? "current"
                                        : item.done
                                        ? "done"
                                        : ""
                                }`}
                                onClick={(e) =>
                                    this.handleNavClick(e, item.title)
                                }
                            >
                                <Index
                                    className={`${
                                        item.active
                                            ? "current"
                                            : item.done
                                            ? "done"
                                            : ""
                                    } ${
                                        wrap &&
                                        currentIndex > index &&
                                        index != 0
                                            ? "previous"
                                            : wrap && currentIndex < index
                                            ? "next"
                                            : ""
                                    }`}
                                >
                                    {item.done &&
                                    index !== currentIndex &&
                                    !wrap
                                        ? <img src={CheckValid} alt=""/>
                                        : wrap &&
                                          index != 0 &&
                                          index != items.length - 1 &&
                                          index != currentIndex
                                        ? "..."
                                        : navItems.indexOf(item.title) + 1}
                                </Index>
                                <Title>
                                    {/*   {wrap && currentIndex > index && index != 0
                                        ? "Previous steps"
                                        : wrap &&
                                          currentIndex < index &&
                                          index != items.length - 1
                                        ? "Other steps"
                                        : item.title} */}
                                    {item.title}
                                </Title>
                            </Circle>
                            {navItems.indexOf(item.title) !==
                            navItems.length - 1 ? (
                                <Bar
                                    id={`bar-${index}`}
                                    className={`${
                                        item.active
                                            ? "current"
                                            : item.done
                                            ? "done"
                                            : ""
                                    }  ${
                                        {
                                            /* wrap && index != 0 ? "hide" : "" */
                                        }
                                    } ${wrap ? "small" : ""}`}
                                />
                            ) : (
                                ""
                            )}
                        </Step>
                    ))}
                </NavBar>
            </Wrapper>
        );
    }
}

export default Stepper;