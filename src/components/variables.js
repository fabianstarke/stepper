// PRIMARY
export const PRIMARY_BASE = '#0036FF'; // blue
export const SECONDARY_BASE = '#FF0266'; // red-pink
export const TERTIARY_BASE = '#FAE345'; // yellow
// GREY SCALE
export const BLACK = '#000000';
export const WHITE = '#FFFFFF';
export const GREY_1 = '#F2F2F2';
export const GREY_2 = '#EBEBEB';
export const GREY_3 = '#878787';
export const GREY_4 = '#505050';
export const GREY_5 = '#6B6B6B';
export const GREY_6 = '#CBCBCB';
// STATUS
export const SUCCESS = '#23C73A';
export const WARNING = '#FFD147';
export const ERROR = '#FF4242';
export const INFO = '#5494F9'; // blue

