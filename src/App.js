import React, { useState } from "react";

import Stepper from './components/Stepper'

function App() {

  const [next, setNext] = useState(0);

  const handleSubmit = (e,i) => setNext(i)
  return (
    <div className="App">
      <header className="App-header">
      <div style={{ position: "relative", width: "100%", display: "flex", alignItems: "center", justifyContent: "center", margin: '400px auto' }}>
                <Stepper
                     currentIndex={next}
                     navItems={["General Settings", "Payouts & Conversion Flow", "Traffic Restrictions", "Descriptions", "Screenshots", "Creatives", "Confirmation"]}
                     onChangePeriodIndex={(e, i) => handleSubmit(e,i)}
                />
            </div>
      </header>
    </div>
  );
}

export default App;
